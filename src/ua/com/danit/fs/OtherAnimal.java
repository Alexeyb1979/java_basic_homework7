package ua.com.danit.fs;

public class OtherAnimal extends Pet {
    private Species species = Species.UNKNOWN;

    public OtherAnimal(String nickname, Species species) {
        super(nickname);
        this.species = species;
    }

    public OtherAnimal(String nickname, int age, int trickLevel, String[] habits, Species species) {
        super(nickname, age, trickLevel, habits);
        this.species = species;
    }

    public OtherAnimal() {

    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + getNickname() + " соскучился!");
    }
}
