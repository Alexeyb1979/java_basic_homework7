package ua.com.danit.fs;

public class RoboCat extends Pet {
    private Species species = Species.ROBO_CAT;
    public RoboCat(String nickname) {
        super(nickname);
        }

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
       }

    public RoboCat() {
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override
    public void respond () { //метод отозваться
        System.out.println("Привет, хозяин. Я - " + getNickname() + " соскучился!");
    }
}
