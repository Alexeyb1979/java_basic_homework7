package ua.com.danit.fs;

public class Fish extends Pet{
    private Species species = Species.FISH;
    public Fish(String nickname) {
        super(nickname);

    }

   public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);

    }

    public Fish() {
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override
    public void respond () { //метод отозваться
        System.out.println("Привет, хозяин. Я - " + getNickname() + " соскучился!");
    }
}
