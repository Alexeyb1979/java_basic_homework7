package ua.com.danit.fs;

public class Main {
    public static void main(String[] args) {

       Pet dog = new Dog ("Bobik");
        System.out.println(dog.getSpecies());

      Pet cat = new DomesticCat("Tomas",5,58, new String[]{"eat", "sleep"});

        Pet rabbit = new OtherAnimal();
        rabbit.setNickname("banny");
        System.out.println(rabbit.getSpecies());

        System.out.println(cat.toString());

        Man father = new Man("Peter", "Yacobs", 1958);
        System.out.println(father.getSchedule()[2][0]);



        Woman bobbiesWife = new Woman("Elisa", "Jonson", 1986);
        Human olderSun = new Human("Bill", "Yacobs", 2000);
        Human youngerSun = new Human("Bob", "Yacobs", 2003);
        Human daughter = new Human("Lisa", "Yacobs", 1998);
        Human nephew = new Human("Tom", "Robson", 1999);
        Human bobbiesNephew = new Human();

        Family family = new Family(bobbiesWife, father);
        family.addChild(olderSun);
        family.addChild(youngerSun);
        family.addChild(daughter);
        family.setPet(cat);
        System.out.println(family.toString());
        family.removeChild(daughter);
        family.removeChild(daughter);

        System.out.println(family.toString());




        family.removeChild(bobbiesNephew);
        System.out.println(family.toString());



        Family[] families = new Family[200000];

//        -Xmx5M to receive OutOfMemoryError
//        for (int i = 0; i < 200000; i++) {
//            families[i] = new Family (bobbiesWife, father);
//        }
    }
}
