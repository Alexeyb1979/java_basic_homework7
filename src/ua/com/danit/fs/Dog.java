package ua.com.danit.fs;

public class Dog extends Pet implements Foul {

    private Species species = Species.DOG;

    public Dog() {
    }

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override
        public void respond () { //метод отозваться
            System.out.println("Привет, хозяин. Я - " + getNickname() + " соскучился!");
    }

    @Override
        public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

}
