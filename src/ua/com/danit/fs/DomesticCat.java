package ua.com.danit.fs;

public class DomesticCat extends Pet implements Foul{

    private Species species = Species.DOMESTIC_CAT;

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    public DomesticCat() {
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + getNickname() + " соскучился!");
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {
        return "DomesticCat{" +
                "species=" + species +
                '}';
    }
}
