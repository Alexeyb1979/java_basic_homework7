package ua.com.danit.fs;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

     @Test
     void testToString() {
         //   - `toString` - проверьте, что методы возвращает определенную строку для
         //   определенных объектов;
         Human father = new Human("Peter", "Yacobs", 1958);
         Human mother = new Human("Elisa", "Jonson", 1986);
         Human sun = new Human("Bill", "Yacobs", 2000);
         Pet cat = new Pet(Pet.Species.CAT,"Tomas",5,58, new String[]{"eat", "sleep"});
         Family family = new Family(mother, father);
         family.addChild(sun);
         family.setPet(cat);
         String expected = "Family {Mother: Human {name = Elisa, surname = Jonson, year = 1986, iq = 0, Father: Human {name = Peter, surname = Yacobs, year = 1958, iq = 0, Children: Human {name = Bill, surname = Yacobs, year = 2000, iq = 0, , pet= CAT {nickname = Tomas, age = 5, trickLevel = 58, habits = [eat, sleep,  ]}}";
         assertEquals(expected, family.toString());
    }
    @Test
    void testAddChild() {
        //  - `addChild` - проверьте, что массив `children` увеличивается на один элемент и что этим элементом
        //  есть именно переданный объект с необходимыми ссылками;
        Human father = new Human("Peter", "Yacobs", 1958);
        Human mother = new Human("Elisa", "Jonson", 1986);
        Human sun = new Human("Bill", "Yacobs", 2000);
        Family family = new Family(mother, father);
        family.addChild(sun);
        int result = family.getChildren().length;
        Human child = family.getChildren()[family.getChildren().length - 1];
        assertEquals(1, result);
        assertEquals(sun, child);
    }


    @Test
    void testRemoveChild() {
//  - проверьте, что ребенок действительно удаляется из массива `children`
//  (если передать объект, эквивалентный хотя бы одному элементу массива);
//  - проверьте, что массив `children` остается без изменений (если передать объект,
//  не эквивалентный ни одному элементу массива);
        Human father = new Human("Peter", "Yacobs", 1958);
        Human mother = new Human("Elisa", "Jonson", 1986);
        Human olderSun = new Human("Bill", "Yacobs", 2000);
        Human youngerSun = new Human("Bob", "Yacobs", 2003);
        Human daughter = new Human("Lisa", "Yacobs", 1998);
        Human nephew = new Human("Tom", "Robson", 1999);
        Family family = new Family(mother, father);
        family.addChild(olderSun);
        family.addChild (youngerSun);
        family.addChild (daughter);
        family.removeChild(daughter);

        int result = family.getChildren().length;
        assertEquals(2, result);

        family.removeChild(nephew);
        int res = family.getChildren().length;
        assertEquals(2, res);

    }

    @Test
    void testCountFamily() {
        //  - `countFamily` - проверьте, что метод возвращает верное количество человек в семье.
        Human father = new Human("Peter", "Yacobs", 1958);
        Human mother = new Human("Elisa", "Jonson", 1986);
        Human olderSun = new Human("Bill", "Yacobs", 2000);
        Human youngerSun = new Human("Bob", "Yacobs", 2003);
        Human daughter = new Human("Lisa", "Yacobs", 1998);
        Human nephew = new Human("Tom", "Robson", 1999);
        Family family = new Family(mother, father);
        family.addChild(olderSun);
        family.addChild (youngerSun);
        family.addChild (daughter);
        int result =  family.countFamily();
        assertEquals(5, result);
    }
}

